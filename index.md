# Berglas PoC using GCP Storage & Secrets Manager

## Table of Contents
- [How it works](#how-it-works)
- [Requirements](#requirements)
- [Write Secrets](#write-secrets)
  - [Write Secrets using GCP Storage and KMS](#write-secrets-using-gcp-storage-and-kms)
  - [Write Secrets using GCP Secrets Manager](#write-secrets-using-gcp-secrets-manager)
- [Read Secrets](#read-secrets)
  - [Read Secrets using Berglas CLI](#read-secrets-using-berglas-cli)
  - [Read Secrets using Terraform](#read-secrets-using-terraform)
  - [Read Secrets (GCS + KMS) using Kubernetes](#read-secrets-gcs-kms-using-kubernetes)
  - [Read Secrets (SM) using Kubernetes](#read-secrets-secrets-manager-using-kubernetes)

## How it works

![berglas-overview](doc/berglas-overview.png)

As shown on the left side of the diagram above, Berglas can manage secrets from **GCP Storage** bucket (encrypted with KMS) and also from a **GCP Secrets Manager** secret. This means we can simplify the storage, encryption & permissions to store and retrieve those secrets, which then - as shown at the right side of the diagram - can be used by several ways to provide their content into our applications.

In this PoC we will experiment on storing secrets on GCP Storage and GCP Secrets Manager, and using them from a Kubernetes Deployment thanks to Terraform scripts, Berglas CLI (i.e. from a CI/CD pipeline) and Kubernetes custom webhooks.

## Requirements

0. Set environment variables:

```shell
export PROJECT_ID=berglas-poc-267411
```

1. Install berglas:

```shell
# in macos
brew install berglas

# in other platforms
docker pull gcr.io/berglas/berglas:latest
```

2. Set our GCP Project in our gcloud CLI:

```shell
gcloud config set project ${PROJECT_ID}
```

3. Get GCP credentials file:

```shell
gcloud auth application-default login
```

4. Enable GCP APIs required by berglas:

```shell
gcloud services enable --project ${PROJECT_ID} \
  cloudkms.googleapis.com \
  storage-api.googleapis.com \
  storage-component.googleapis.com \
  secretmanager.googleapis.com \
  container.googleapis.com \
  cloudfunctions.googleapis.com
```

## Write Secrets

In this section we will explore two options to store secrets on GCP using Berglas: GCP Storage and GCP Secrets Manager. Bear in mind that these options are not exclusive, meaning that we can choose to use only one option or both at the same time.

### Write Secrets using GCP Storage and KMS

This method will set up a GCS bucket to store our secret data. Berglas will encrypt this data at rest, using a KMS Key.

0. Set environment variables:

```shell
export PROJECT_ID=berglas-poc-267411
export BUCKET_ID=${PROJECT_ID}-secrets
export KEY_NAME=projects/${PROJECT_ID}/locations/global/keyRings/berglas/cryptoKeys/berglas-key
export SECRET_NAME=myApp/production/jwtToken
```

1. Create GCS Bucket for storing secrets:

```shell
gsutil ls -b gs://${BUCKET_ID} || (gcloud services enable storage-api.googleapis.com && gsutil mb gs://${BUCKET_ID} && gsutil versioning set on gs://${BUCKET_ID})
```

2. Initialize secrets storage using Berglas:

```shell
berglas bootstrap --project $PROJECT_ID --bucket $BUCKET_ID
```

3. Create secret:

```shell
berglas create ${BUCKET_ID}/${SECRET_NAME} abcd1234 \
    --key ${KEY_NAME}
```

4. Update secret:

```shell
berglas update ${BUCKET_ID}/${SECRET_NAME} abcd9876 \
    --key ${KEY_NAME}
```

5. Access (read) secret:

```shell
berglas access ${BUCKET_ID}/${SECRET_NAME}
# abcd9876%
```

5. Grant access to other members:

```shell
berglas grant ${BUCKET_ID}/${SECRET_NAME} --member user:someone@example.com
```

6. Revoke access to other members:

```shell
berglas revoke ${BUCKET_ID}/${SECRET_NAME} --member user:someone@example.com
```

### Write Secrets using GCP Secrets Manager

This method will set up a GCP Secrets Manager object to store our secret data. Encryption is internally handled by Secrets Manager, and Berglas will only take care or creating, updating and removing those objects. As documented at the [release page](https://cloud.google.com/blog/products/identity-security/introducing-google-clouds-secret-manager), the only difference for Berglas to use Secrets Manager instead of GCS is to use `sm://` prefix into our secret name, and ignore the KMS key while creating/updating a secret.

0. Set environment variables:

```shell
export PROJECT_ID=berglas-poc-267411
export SECRET_NAME=myApp-production-jwtToken
```

3. Create secret:

```shell
berglas create sm://${PROJECT_ID}/${SECRET_NAME} abcd1234
```

4. Update secret:

```shell
berglas update sm://${PROJECT_ID}/${SECRET_NAME} abcd9876
```

5. Access (read) secret:

```shell
berglas access sm://${PROJECT_ID}/${SECRET_NAME}
# abcd9876%
```

5. Grant access to other members:

```shell
berglas grant sm://${PROJECT_ID}/${SECRET_NAME} --member user:someone@example.com
```

6. Revoke access to other members:

```shell
berglas revoke sm://${PROJECT_ID}/${SECRET_NAME} --member user:someone@example.com
```

## Read Secrets

### Read Secrets using Berglas CLI

![read-secrets-storage-cli](doc/read-secrets-storage-cli.png)

As shown in the sections above, the easiest way to read a secret is using `berglas access` command:

```shell
# from a GCS Bucket object
export PROJECT_ID=berglas-poc-267411
export BUCKET_ID=${PROJECT_ID}-secrets
export SECRET_NAME=myApp/production/jwtToken
berglas access ${BUCKET_ID}/${SECRET_NAME}
# abcd9876%

# from a Secrets Manager object
export PROJECT_ID=berglas-poc-267411
export SECRET_NAME=myApp-production-jwtToken
berglas access sm://${PROJECT_ID}/${SECRET_NAME}
# abcd9876%
```

The cool part is that we can put these steps in a CI/CD pipeline job, in order to automate this execution and hand-over these secrets to other components. For example, using a GitlabCI job it would look like this:

```yaml
create-secret:
  image: gcr.io/berglas/berglas:latest
  variables:
    PROJECT_ID: berglas-poc-267411
    SECRET_NAME: myApp-production-jwtToken
    # GCP_ACCOUNT_CREDENTIALS: defined as secret CI/CD variable
  before_script:
    - echo ${GCP_ACCOUNT_CREDENTIALS} > account.json
    - gcloud auth activate-service-account --key-file=account.json --project=${PROJECT_ID}
  script:
    - berglas access sm://${PROJECT_ID}/${SECRET_NAME}
    # . . .
```

### Read Secrets (GCS + KMS) using Terraform

![read-secrets-storage-terraform](doc/read-secrets-storage-terraform.png)

**Note**: as of the date of this document, [terraform-provider-berglas](https://github.com/sethvargo/terraform-provider-berglas) provider has some limitations related to Berglas functionalities:

- Berglas `grant` actions are not yet supported.
- GCP Secrets Manager is not yet supported.

If none of these points affect your intgrations, we recommend you to use this approach instead of using Berglas CLI.

0. Set environment variables:

```sh
export PROJECT_ID=berglas-poc-267411
export BUCKET_ID=${PROJECT_ID}-secrets
export KEY_NAME=projects/${PROJECT_ID}/locations/global/keyRings/berglas/cryptoKeys/berglas-key
export SECRET_NAME=myApp/production/jwtToken
```

1. Install terraform and `terraform-provider-berglas` provider:

```shell
# install terraform
brew install terraform

# install terraform-provider-berglas
mkdir -p $HOME/.terraform.d/plugins
wget -O terraform-provider-berglas.tgz https://github.com/sethvargo/terraform-provider-berglas/releases/download/v0.0.1/darwin_amd64.tgz
tar -zxf terraform-provider-berglas.tgz
mv darwin_amd64/terraform-provider-berglas $HOME/.terraform.d/plugins
rm -f terraform-provider-berglas.tgz
rm -rf darwin_amd64
```

2. Create Secret:

```shell
# create secrets bucket
gsutil ls -b gs://${BUCKET_ID} || (gcloud services enable storage-api.googleapis.com && gsutil mb gs://${BUCKET_ID} && gsutil versioning set on gs://${BUCKET_ID})

# initialize berglas into this bucket
berglas bootstrap --project $PROJECT_ID --bucket $BUCKET_ID

# create secret
berglas create ${BUCKET_ID}/${SECRET_NAME} abcd1234 \
    --key ${KEY_NAME}
```

3. Read Secret:

See `terraform/` folder for sample file:

```terraform
data "berglas_secret" "jwtToken" {
  bucket = "berglas-poc-267411-secrets"
  name   = "myApp/production/jwtToken"
}

output "jwtTokenValue" {
  value = "${data.berglas_secret.jwtToken.plaintext}"
}
```

### Read Secrets (GCS + KMS) using Kubernetes

![read-secrets-storage-webhook](doc/read-secrets-storage-webhook.png)

**Note**: as [documented](https://github.com/GoogleCloudPlatform/berglas/tree/master/examples/kubernetes#limitations) at the official website, this method requires to define a `command` in our container spec. That's because the webhook will mutate the spec in order to use `berglas exec` with our own command, injecting all defined secrets inside our container process.

0. Set environment variables:

```sh
export PROJECT_ID=berglas-poc-267411
export BUCKET_ID=${PROJECT_ID}-secrets
export SA_EMAIL=berglas-k8s@${PROJECT_ID}.iam.gserviceaccount.com
export KEY_NAME=projects/${PROJECT_ID}/locations/global/keyRings/berglas/cryptoKeys/berglas-key
export SECRET_NAME=myApp/production/jwtToken
export JWT_TOKEN_OBJECT_URL=berglas://${BUCKET_ID}/${SECRET_NAME}
```

1. Create Secret:

```shell
# create secrets bucket
gsutil ls -b gs://${BUCKET_ID} || (gcloud services enable storage-api.googleapis.com && gsutil mb gs://${BUCKET_ID} && gsutil versioning set on gs://${BUCKET_ID})

# initialize berglas into this bucket
berglas bootstrap --project $PROJECT_ID --bucket $BUCKET_ID

# create secret
berglas create ${BUCKET_ID}/${SECRET_NAME} abcd1234 \
    --key ${KEY_NAME}
```

2. Create Kubernetes Cluster:

```sh
# create service account
gcloud iam service-accounts create berglas-k8s \
  --project ${PROJECT_ID} \
  --display-name "Berglas K8S account"

# grant k8s permissions to the SA
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${SA_EMAIL}" \
  --role roles/logging.logWriter
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${SA_EMAIL}" \
  --role roles/monitoring.metricWriter
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${SA_EMAIL}" \
  --role roles/monitoring.viewer

# grant berglas GCS permissions to the SA
berglas grant ${BUCKET_ID}/${SECRET_NAME} --member serviceAccount:${SA_EMAIL}

# create a k8s cluster
gcloud container clusters create my-services-cluster \
    --zone us-central1 \
    --cluster-version 1.13 \
    --num-nodes 1 \
    --preemptible \
    --machine-type n1-standard-1 \
    --service-account ${SA_EMAIL}

# get cluster credentials
gcloud container clusters get-credentials my-services-cluster --region us-central1
```

3. Create Webhook as Cloud Function

```shell
# go to webhook source code folder
cd kubernetes

# deploy cloud function
gcloud functions deploy berglas-secrets-webhook \
  --project ${PROJECT_ID} \
  --runtime go111 \
  --entry-point F \
  --trigger-http

# get cloud function endpoint
ENDPOINT=$(gcloud functions describe berglas-secrets-webhook --project ${PROJECT_ID} --format 'value(httpsTrigger.url)')
```

4. Deploy MutatingWebhookConfiguration in Kubernetes

```shell
sed "s|REPLACE_WITH_YOUR_URL|$ENDPOINT|" deploy/webhook.yaml | kubectl apply -f -
```

5. Deploy sample app using secret environment variables

```shell
sed "s|JWT_TOKEN_OBJECT_URL|$JWT_TOKEN_OBJECT_URL|" deploy/sample.yaml | kubectl apply -f -
```

This command will set up `JWT_TOKEN` variable value inside `deploy/sample.yaml` file, that will result in a pod definition that contains:

```yaml
env:
- name: JWT_TOKEN_TEMPFILE
  value: berglas://berglas-poc-267411-secrets/myApp/production/jwtToken?destination=/tmp/jwtToken
- name: JWT_TOKEN
  value: berglas://berglas-poc-267411-secrets/myApp/production/jwtToken
```

Which means two things for the pod:

- The container will *mount* the secret content in `/tmp/jwtToken` file.

- The container process (specified on its `command`) will have an environment variable called `JWT_TOKEN` with the secret value (this magic happens thanks to the mutating webhook). There are a few ways to check this:

  - Looking at the PID environment variables:
    ```shell
    # get deployment pod
    ➜  kubectl get pod
    NAME                         READY   STATUS    RESTARTS   AGE
    envserver-64df79bd4d-p9crr   1/1     Running   0          56m

    # connect to pod
    ➜  kubectl exec -it envserver-64df79bd4d-p9crr -- sh

    # get envserver process id
    / $ ps -fea
    PID   USER     TIME  COMMAND
        1 appuser   0:00 /berglas/bin/berglas exec -- /bin/envserver
       12 appuser   0:00 /bin/envserver
       18 appuser   0:00 sh
       23 appuser   0:00 ps -fea

    # get process environment variables
    / $ cat /proc/12/environ
    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/binHOSTNAME=envserver-64df79bd4d-p9crrJWT_TOKEN=abcd1234ENVSERVER_PORT_80_TCP_PORT=80KUBERNETES_PORT_443_TCP=tcp://10.59.240.1:443ENVSERVER_PORT_80_TCP=tcp://10.59.255.198:80ENVSERVER_PORT_80_TCP_PROTO=tcpKUBERNETES_PORT=tcp://10.59.240.1:443ENVSERVER_SERVICE_PORT=80ENVSERVER_PORT_80_TCP_ADDR=10.59.255.198ENVSERVER_SERVICE_HOST=10.59.255.198ENVSERVER_PORT=tcp://10.59.255.198:80KUBERNETES_SERVICE_PORT_HTTPS=443KUBERNETES_PORT_443_TCP_PROTO=tcpKUBERNETES_PORT_443_TCP_PORT=443KUBERNETES_PORT_443_TCP_ADDR=10.59.240.1KUBERNETES_SERVICE_HOST=10.59.240.1KUBERNETES_SERVICE_PORT=443HOME=/home/appuser/

    # take a look into JWT_TOKEN variable
    ```

  - Using `sethvargo/envserver` HTTP endpoint:
    ```shell
    # get loadbalancer IP address
    ➜  kubectl get svc
    NAME         TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE
    envserver    LoadBalancer   10.59.255.198   35.232.127.201   80:32626/TCP   163m
    kubernetes   ClusterIP      10.59.240.1     <none>           443/TCP        170m

    # curl load balancer from the outside world
    ➜  curl 35.232.127.201
    {"ENVSERVER_PORT":"tcp://10.59.255.198:80","ENVSERVER_PORT_80_TCP":"tcp://10.59.255.198:80","ENVSERVER_PORT_80_TCP_ADDR":"10.59.255.198","ENVSERVER_PORT_80_TCP_PORT":"80","ENVSERVER_PORT_80_TCP_PROTO":"tcp","ENVSERVER_SERVICE_HOST":"10.59.255.198","ENVSERVER_SERVICE_PORT":"80","HOME":"/home/appuser","HOSTNAME":"envserver-64df79bd4d-p9crr","JWT_TOKEN":"abcd1234","KUBERNETES_PORT":"tcp://10.59.240.1:443","KUBERNETES_PORT_443_TCP":"tcp://10.59.240.1:443","KUBERNETES_PORT_443_TCP_ADDR":"10.59.240.1","KUBERNETES_PORT_443_TCP_PORT":"443","KUBERNETES_PORT_443_TCP_PROTO":"tcp","KUBERNETES_SERVICE_HOST":"10.59.240.1","KUBERNETES_SERVICE_PORT":"443","KUBERNETES_SERVICE_PORT_HTTPS":"443","PATH":"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"}

    # take a look into JWT_TOKEN variable
    ```

Please refer to [this link](https://github.com/GoogleCloudPlatform/berglas/blob/master/doc/reference-syntax.md) for more information on berglas URL syntax.

### Read Secrets (Secrets Manager) using Kubernetes

![read-secrets-sm-webhook](doc/read-secrets-sm-webhook.png)

**Note**: Please be aware that GCP Secrets Manager is a fairly new product (as of the date of this document) and doesn't seem to support (yet) all same features as GCS + KMS. On the other hand, this method has the same `command` limitations as using GCS and KMS, since the mutating webhook configuration remains the same.

0. Set environment variables:

```sh
export PROJECT_ID=berglas-poc-267411
export SA_EMAIL=berglas-k8s@${PROJECT_ID}.iam.gserviceaccount.com
export SECRET_NAME=myApp-production-jwtToken
export JWT_TOKEN_OBJECT_URL=sm://${PROJECT_ID}/${SECRET_NAME}
```

1. Create Secret:

```shell
# create secret
berglas create sm://${PROJECT_ID}/${SECRET_NAME} abcd9999
```

2. Create Kubernetes Cluster:

```sh
# create service account
gcloud iam service-accounts create berglas-k8s \
  --project ${PROJECT_ID} \
  --display-name "Berglas K8S account"

# grant k8s permissions to the SA
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${SA_EMAIL}" \
  --role roles/logging.logWriter
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${SA_EMAIL}" \
  --role roles/monitoring.metricWriter
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --member "serviceAccount:${SA_EMAIL}" \
  --role roles/monitoring.viewer

# grant berglas SM permissions to the SA
berglas grant sm://${PROJECT_ID}/${SECRET_NAME} --member serviceAccount:${SA_EMAIL}

# create a k8s cluster
gcloud container clusters create my-services-cluster \
    --zone us-central1 \
    --cluster-version 1.13 \
    --num-nodes 1 \
    --preemptible \
    --machine-type n1-standard-1 \
    --service-account ${SA_EMAIL}

# get cluster credentials
gcloud container clusters get-credentials my-services-cluster --region us-central1
```

3. Create Webhook as Cloud Function

```shell
# go to webhook source code folder
cd kubernetes

# deploy cloud function
gcloud functions deploy berglas-secrets-webhook \
  --project ${PROJECT_ID} \
  --runtime go111 \
  --entry-point F \
  --trigger-http

# get cloud function endpoint
ENDPOINT=$(gcloud functions describe berglas-secrets-webhook --project ${PROJECT_ID} --format 'value(httpsTrigger.url)')
```

4. Deploy MutatingWebhookConfiguration in Kubernetes

```shell
sed "s|REPLACE_WITH_YOUR_URL|$ENDPOINT|" deploy/webhook.yaml | kubectl apply -f -
```

5. Deploy sample app using secret environment variables

```shell
sed "s|JWT_TOKEN_OBJECT_URL|$JWT_TOKEN_OBJECT_URL|" deploy/sample.yaml | kubectl apply -f -
```

This command will set up `JWT_TOKEN` variable value inside `deploy/sample.yaml` file, that will result in a pod definition that contains:

```yaml
env:
- name: JWT_TOKEN_TEMPFILE
  value: sm://berglas-poc-267411/myApp-production-jwtToken?destination=/tmp/jwtToken
- name: JWT_TOKEN
  value: sm://berglas-poc-267411/myApp-production-jwtToken
```

Which will have the same result as the GCS experiment, but using GCP Secrets Manager thanks to using `sm://` prefix instead of `berglas://`:

```shell
# get loadbalancer IP address
➜  kubectl get svc
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE
envserver    LoadBalancer   10.59.255.198   35.232.127.201   80:32626/TCP   3h56m
kubernetes   ClusterIP      10.59.240.1     <none>           443/TCP        4h3m

# get loadbalancer IP address
➜  curl 35.232.127.201
{"ENVSERVER_PORT":"tcp://10.59.255.198:80","ENVSERVER_PORT_80_TCP":"tcp://10.59.255.198:80","ENVSERVER_PORT_80_TCP_ADDR":"10.59.255.198","ENVSERVER_PORT_80_TCP_PORT":"80","ENVSERVER_PORT_80_TCP_PROTO":"tcp","ENVSERVER_SERVICE_HOST":"10.59.255.198","ENVSERVER_SERVICE_PORT":"80","HOME":"/home/appuser","HOSTNAME":"envserver-7c56c5fc9d-kxnth","JWT_TOKEN":"abcd9999","JWT_TOKEN_TEMPFILE":"/tmp/jwtToken","KUBERNETES_PORT":"tcp://10.59.240.1:443","KUBERNETES_PORT_443_TCP":"tcp://10.59.240.1:443","KUBERNETES_PORT_443_TCP_ADDR":"10.59.240.1","KUBERNETES_PORT_443_TCP_PORT":"443","KUBERNETES_PORT_443_TCP_PROTO":"tcp","KUBERNETES_SERVICE_HOST":"10.59.240.1","KUBERNETES_SERVICE_PORT":"443","KUBERNETES_SERVICE_PORT_HTTPS":"443","PATH":"/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"}

# take a look into JWT_TOKEN variable
```
