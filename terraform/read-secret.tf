data "berglas_secret" "jwtToken" {
  bucket = "berglas-poc-267313-secrets"
  name   = "myApp/production/jwtToken"
}

output "jwtTokenValue" {
  value = "${data.berglas_secret.jwtToken.plaintext}"
}
